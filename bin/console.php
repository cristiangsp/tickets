#!/usr/bin/env php
<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Tickets\Infrastructure\Persistence\Doctrine\Fixtures\LoadFixturesCommand;

$app = \Tickets\Infrastructure\UI\Web\Silex\Application::bootstrap();

$cli = ConsoleRunner::createApplication(ConsoleRunner::createHelperSet($app['orm.em']));
$cli->add(new LoadFixturesCommand($app['orm.em']));
$cli->run();
