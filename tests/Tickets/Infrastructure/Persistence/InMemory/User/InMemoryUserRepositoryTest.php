<?php

namespace Tickets\Infrastructure\Persistence\InMemory\User;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\User\User;

class InMemoryUserRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function userIsSavedProperly()
    {
        $userRepository = new InMemoryUserRepository();
        $userRepository->save(new User("John"));
        $this->assertCount(1, $userRepository->all());
    }

    /**
     * @test
     */
    public function userIsFoundByIdProperly()
    {
        $userRepository = new InMemoryUserRepository();

        $user = new User("John");
        $userRepository->save($user);
        $this->assertEquals($user, $userRepository->ofIdOrFail($user->id()));
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\User\DoesNotExistException
     */
    public function notExistingUserFails()
    {
        $userRepository = new InMemoryUserRepository();
        $userRepository->ofIdOrFail(1);
    }
}

