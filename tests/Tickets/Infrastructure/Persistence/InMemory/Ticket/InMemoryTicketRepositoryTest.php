<?php

namespace Tickets\Infrastructure\Persistence\InMemory\Ticket;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\Ticket\Ticket;

class InMemoryTicketRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function ticketIsSavedProperly()
    {
        $ticketRepository = new InMemoryTicketRepository();
        $ticketRepository->save(new Ticket(new Listing(new User("John"), 600, "Coldplay")));
        $this->assertCount(1, $ticketRepository->all());
    }

}
