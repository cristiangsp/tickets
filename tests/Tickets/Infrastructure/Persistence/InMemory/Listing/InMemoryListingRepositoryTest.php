<?php

namespace Tickets\Infrastructure\Persistence\InMemory\Listing;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;

class InMemoryListingRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function listingIsSavedProperly()
    {
        $listingRepository = new InMemoryListingRepository();
        $listingRepository->save(new Listing(new User("John"), 600, "Coldplay"));
        $this->assertCount(1, $listingRepository->all());
    }
}
