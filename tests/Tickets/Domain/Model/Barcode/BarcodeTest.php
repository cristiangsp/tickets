<?php

namespace Tickets\Domain\Model\Barcode\Barcode;

use Tickets\Domain\Model\Barcode\Barcode;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\Ticket\Ticket;
use PHPUnit\Framework\TestCase;

class BarcodeTest extends TestCase
{
    /**
     * @test
     */
    public function aBarcodeIsCreatedProperly()
    {
        $ticket = new Ticket(new Listing(new User("John"), 600, "Coldplay"));
        $barcode = new Barcode($ticket, "EAN-13:111111111");
        $this->assertEquals($ticket, $barcode->ticket());
        $this->assertEquals("EAN-13:111111111", $barcode->barcode());
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\Barcode\EmptyBarCodeException
     */
    public function aBarcodeCannotHaveAnEmptyBarCode()
    {
        $barcode = new Barcode(
            new Ticket(new Listing(new User("John"), 600, "Coldplay")),
            ""
        );
    }
}
