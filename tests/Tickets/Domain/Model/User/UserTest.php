<?php

namespace Tickets\Domain\Model\User;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     */
    public function aUserIsCreatedProperly()
    {
        $user = new User("John");
        $this->assertEquals("John", $user->name());
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\User\EmptyNameException
     */
    public function aUserNameCannotBeEmpty()
    {
        $user = new User("");
    }
}
