<?php

namespace Tickets\Domain\Model\Listing;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\User\User;

class ListingTest extends TestCase
{
    /**
     * @test
     */
    public function aListingIsCreatedProperly()
    {
        $user = new User("John");
        $listing = new Listing($user, 600, "Coldplay");
        $this->assertEquals($listing->sellingPrice(), 600);
        $this->assertEquals($listing->description(), "Coldplay");
        $this->assertEquals($listing->author(), $user);
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\Listing\NegativeSellingPriceException
     */
    public function aListingWithNegativeSellingPriceIsNotAllowed()
    {
        $listing = new Listing(new User("John"), -5, "Coldplay");
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\Listing\EmptyDescriptionException
     */
    public function aListingWithEmptyDescriptionIsNotAllowed()
    {
        $listing = new Listing(new User("John"), 500, "");
    }
}
