<?php

namespace Tickets\Domain\Model\Ticket;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;

class TicketTest extends TestCase
{
    /**
     * @test
     */
    public function aTicketIsCreatedProperly()
    {
        $listing = new Listing(new User("John"), 600, "Coldplay");
        $ticket = new Ticket($listing);
        $this->assertEquals($listing, $ticket->listing());
    }

    /**
     * @test
     */
    public function aTicketIsPurchasedProperly()
    {
        $ticket = new Ticket(new Listing(new User("John"), 600, "Coldplay"));

        $buyer = new User(" Tom");
        $ticket->purchase($buyer, new \DateTime("now"));

        $this->assertEquals($buyer, $ticket->lastPurchase()->boughtByUser());
        $this->assertInstanceOf(\DateTime::class, $ticket->lastPurchase()->boughtAtDate());
    }
}
