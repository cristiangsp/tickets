<?php

namespace Tickets\Application\Command\Barcode;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\Barcode\BarcodeRepository;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\Ticket\Ticket;
use Tickets\Domain\Model\Ticket\TicketRepository;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\User\UserRepository;
use Tickets\Infrastructure\Persistence\InMemory\Barcode\InMemoryBarcodeRepository;
use Tickets\Infrastructure\Persistence\InMemory\Ticket\InMemoryTicketRepository;
use Tickets\Infrastructure\Persistence\InMemory\User\InMemoryUserRepository;
use Tickets\Domain\Model\Barcode\AlreadyExistsException;

class CreateBarcodeHandlerTest extends TestCase
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     * @var BarcodeRepository
     */
    private $barcodeRepository;

    /**
     *
     */
    public function setUp()
    {
        $this->userRepository = new InMemoryUserRepository();
        $this->barcodeRepository = new InMemoryBarcodeRepository();
        $this->ticketRepository = new InMemoryTicketRepository();
    }

    /**
     * @test
     */
    public function barcodeIsCreatedProperly()
    {
        $user = new User("John");
        $this->userRepository->save($user);

        $ticket = new Ticket(new Listing($user, 600, "ColdPlay"));
        $this->ticketRepository->save($ticket);

        $handler = new CreateBarcodeHandler(
            $this->ticketRepository,
            $this->barcodeRepository
        );

        $handler->handle(new CreateBarcodeCommand(
            $user->id(),
            $ticket->id(),
            "EAN-13:444444444",
            2
        ));

        $this->assertCount(1, $this->barcodeRepository->all());
    }

    /**
     * @test
     * @expectedException Tickets\Domain\Model\Barcode\AlreadyExistsException
     */
    public function AnAlreadyExistingBarcodeIsNotAllowed()
    {
        $user = new User("John");
        $this->userRepository->save($user);

        $ticket = new Ticket(new Listing($user, 600, "ColdPlay"));
        $this->ticketRepository->save($ticket);

        $handler = new CreateBarcodeHandler(
            $this->ticketRepository,
            $this->barcodeRepository
        );

        $handler->handle(new CreateBarcodeCommand(
            $user->id(),
            $ticket->id(),
            "EAN-13:444444444"
        ));

        $handler->handle(new CreateBarcodeCommand(
            $user->id(),
            $ticket->id(),
            "EAN-13:444444444"
        ));
    }

    /**
     * @test
     */
    public function AddingAnExistingBarcodeIsAllowedIfItWasBoughtByTheUser()
    {
        $user = new User("John");
        $this->userRepository->save($user);

        $ticket = new Ticket(new Listing($user, 600, "ColdPlay"));
        $this->ticketRepository->save($ticket);

        $handler = new CreateBarcodeHandler(
            $this->ticketRepository,
            $this->barcodeRepository
        );

        $handler->handle(new CreateBarcodeCommand(
            $user->id(),
            $ticket->id(),
            "EAN-13:444444444"
        ));

        $buyer = new User("Frank");
        $this->userRepository->save($buyer);

        $ticket->purchase($buyer, new \DateTime());

        $ticket = new Ticket(new Listing($user, 650, "ColdPlay"));
        $this->ticketRepository->save($ticket);

        $handler->handle(new CreateBarcodeCommand(
            $buyer->id(),
            $ticket->id(),
            "EAN-13:444444444"
        ));

        $this->assertCount(2, $this->barcodeRepository->all());
    }
}
