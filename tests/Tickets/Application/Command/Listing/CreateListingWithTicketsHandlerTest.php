<?php

namespace Tickets\Application\Command\Listing;

use PHPUnit\Framework\TestCase;
use Tickets\Domain\Model\Listing\ListingRepository;
use Tickets\Domain\Model\Ticket\TicketRepository;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\User\UserRepository;
use Tickets\Infrastructure\Persistence\InMemory\Listing\InMemoryListingRepository;
use Tickets\Infrastructure\Persistence\InMemory\Ticket\InMemoryTicketRepository;
use Tickets\Infrastructure\Persistence\InMemory\User\InMemoryUserRepository;

class CreateListingWithTicketsHandlerTest extends TestCase
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ListingRepository
     */
    private $listingRepository;

    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     *
     */
    public function setUp()
    {
        $this->userRepository = new InMemoryUserRepository();
        $this->listingRepository = new InMemoryListingRepository();
        $this->ticketRepository = new InMemoryTicketRepository();
    }

    /**
     * @test
     */
    public function listingWithTicketsIsCreatedProperly()
    {
        $this->userRepository->save(new User("John"));

        $handler = new CreateListingWithTicketsHandler(
            $this->userRepository,
            $this->listingRepository,
            $this->ticketRepository
        );

        $listing = $handler->handle(new CreateListingWithTicketsCommand(
            1,
            600,
            "ColdPlay",
            2
        ));

        $this->assertCount(1, $this->listingRepository->all());

        $tickets = $this->ticketRepository->all();
        $this->assertCount(2, $tickets);

        foreach ($tickets as $ticket) {
            $this->assertEquals($listing, $ticket->listing());
        }
    }
}
