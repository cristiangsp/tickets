## Solution

### Domain
The solution have been built on top of the following Domain concepts:

#### Value Objects
A value object for representing the purchase of a ticket is created and used in the Ticket Entity.

|Value Object| Description|
|---|---|
| Purchase   |  Represents a purchase of a Ticket. Included the buyer user and the date  |

#### Entities:
Four entities have been created to represent the four main concepts of the domain:

|Entity| Description|
|---|---|
| User   |  Represents a user of the system  |
| Listing   |  Represents a listing in the system.  |
| Ticket   |  Represents a ticket associated to a Listing  |
| Barcode   |  Represents a Barcode associated to a Ticket  |

All the Entities form their own Aggregate so although there is a relationship between them, they are treated as independent systems.

#### Invariants:
The Domain keeps the following invariants preserved:

About listings:

- A Listing cannot have its description empty.
- A Listing cannot have a negative selling price.
- A Listing must have a least one ticket when created.

About barcodes:

- Barcodes must be unique in the system unless the barcode was bought in the past by the user that adds it.

### Application
The Application offers the following commands:

|Command| Description|
|---|---|
| Create a listing with tickets   |  Creates a new Listing with the number of tickets specified.  |
| Create a Barcode   |  Adds a new Barcode to a Ticket.  |

### Infrastructure
The Persistence is implemented in memory for testing purposes and against MySQL through Doctrine.  

The UI is implemented through a Silex application that includes several providers (Form, Twig, Security, Doctrine, Tactician) adapted to follow the assestment requirements.

### Usage

#### Dependecies
- git
- PHP 7.1.9
- Composer
- Docker

#### Installation
Clone git repository:

```
$> git clone git@bitbucket.org:cristiangsp/tickets.git
```

Install composer depenencies:

```
$> cd tickets
$> composer install
```

Create Docker containers

```
$> docker-compose up -d
```

Generate MySQL schema

```
$> docker-compose exec php /var/www/tickets/bin/console.php orm:schema-tool:create
```

Generate Assestment Fixtures

```
$> docker-compose exec php /var/www/tickets/bin/console.php app:load-data-fixtures
```
#### Execution

To enter the application visit http://127.0.0.1:8080

Authenticate using a username from the example dataset.

### Improvements
- Domain Events have not been implement in the solution.
- Integration tests for the Doctrine Repositories should be added.
- Functional testing to ensure that the system performs properly as a whole should be added. For example, using Behat and Mink.
- The sample data was coupling the id generation to the database, maybe make sense a refactor of that so we can decouple this and avoid be attached to a specific database.