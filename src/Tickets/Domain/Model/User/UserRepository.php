<?php

namespace Tickets\Domain\Model\User;

interface UserRepository
{
    /**
     * @param $id
     * @return User
     */
    public function ofIdOrFail($id) : User;

    /**
     * @param $name
     * @return User
     */
    public function ofNameOrFail($name) : User;

    /**
     * @param User $user
     */
    public function save(User $user) : void;

    /**
     * @return array
     */
    public function all(): array;
}