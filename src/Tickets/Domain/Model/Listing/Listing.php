<?php

namespace Tickets\Domain\Model\Listing;

use Tickets\Domain\Model\User\User;

class Listing
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $author;

    /**
     * @var int
     */
    private $sellingPrice;

    /**
     * @var string
     */
    private $description;

    /**
     * Listing constructor.
     * @param User $author
     * @param int $sellingPrice
     * @param string $description
     */
    public function __construct(User $author, int $sellingPrice, string $description)
    {
        $this->author = $author;
        $this->setSellingPrice($sellingPrice);
        $this->setDescription($description);
    }

    /**
     * @param int $sellingPrice
     * @throws NegativeSellingPriceException
     */
    private function setSellingPrice(int $sellingPrice): void
    {
        if ($sellingPrice < 0) {
            throw new NegativeSellingPriceException();
        }

        $this->sellingPrice = $sellingPrice;
    }

    /**
     * @param string $description
     * @throws EmptyDescriptionException
     */
    private function setDescription(string $description): void
    {
        if (empty($description)) {
            throw new EmptyDescriptionException();
        }

        $this->description = $description;
    }

    /**
     * @return int
     */
    public function id() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function author() : User
    {
        return $this->author;
    }

    /**
     * @return int
     */
    public function sellingPrice() : int
    {
        return $this->sellingPrice;
    }

    /**
     * @return string
     */
    public function description() : string
    {
        return $this->description;
    }
}