<?php

namespace Tickets\Domain\Model\Listing;

interface ListingRepository
{
    /**
     * @param $id
     * @return Listing
     */
    public function ofIdOrFail($id) : Listing;

    /**
     * @param int $userId
     * @return array
     */
    public function ofUserId(int $userId) : array;

    /**
     * @param Listing $listing
     */
    public function save(Listing $listing) : void;

    /**
     * @return array
     */
    public function all(): array;
}