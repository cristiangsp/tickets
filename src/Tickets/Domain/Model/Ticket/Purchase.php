<?php

namespace Tickets\Domain\Model\Ticket;

use Tickets\Domain\Model\User\User;

class Purchase
{
    /**
     * @var User
     */
    private $boughtByUser;

    /**
     * @var \DateTime
     */
    private $boughtAtDate;

    /**
     * Purchase constructor.
     * @param User $buyer
     * @param \DateTime $at
     */
    public function __construct(User $buyer, \DateTime $at)
    {
        $this->boughtByUser = $buyer;
        $this->boughtAtDate = $at;
    }

    /**
     * @return User
     */
    public function boughtByUser() : User
    {
        return $this->boughtByUser;
    }

    /**
     * @return \DateTime
     */
    public function boughtAtDate() : \DateTime
    {
        return $this->boughtAtDate;
    }
}
