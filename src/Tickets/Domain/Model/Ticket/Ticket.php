<?php

namespace Tickets\Domain\Model\Ticket;

use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;

class Ticket
{
    /**
     * @var id
     */
    private $id;

    /**
     * @var Listing
     */
    private $listing;

    /**
     * @var User
     */
    private $boughtByUser;

    /**
     * @var \DateTime
     */
    private $boughtAtDate;

    /**
     * Ticket constructor.
     * @param Listing $listing
     */
    public function __construct(Listing $listing)
    {
        $this->listing = $listing;
    }

    /**
     * @return int
     */
    public function id() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Listing
     */
    public function listing() : Listing
    {
        return $this->listing;
    }

    /**
     * @return Purchase
     */
    public function lastPurchase() : Purchase
    {
        return new Purchase($this->boughtByUser, $this->boughtAtDate);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function wasBoughtBy(int $userId)
    {
        if (!$this->boughtByUser) {
            return false;
        }

        return $userId === $this->boughtByUser->id();
    }

    /**
     * @param User $buyer
     */
    public function purchase(User $buyer, \DateTime $at)
    {
        $this->boughtByUser = $buyer;
        $this->boughtAtDate = $at;
    }
}