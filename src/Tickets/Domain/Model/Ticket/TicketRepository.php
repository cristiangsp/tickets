<?php

namespace Tickets\Domain\Model\Ticket;

interface TicketRepository
{
    /**
     * @param int $ticketId
     * @return Ticket
     */
    public function ofIdOrFail(int $ticketId) : Ticket;

    /**
     * @param int $listingId
     * @return array
     */
    public function ofListingId(int $listingId) : array;

    /**
     * @param Ticket $ticket
     */
    public function save(Ticket $ticket) : void;

    /**
     * @return array
     */
    public function all() : array;
}