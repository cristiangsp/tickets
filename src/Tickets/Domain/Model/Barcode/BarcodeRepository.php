<?php

namespace Tickets\Domain\Model\Barcode;

interface BarcodeRepository
{
    /**
     * @param $id
     * @return Barcode
     */
    public function ofIdOrFail($id) : Barcode;

    /**
     * @param int $ticketId
     * @return array
     */
    public function ofTicketId(int $ticketId) : array;

    /**
     * @param $barcode
     * @param $userId
     * @return mixed
     */
    public function withCodeAndNotBoughtByUserId($barcode, $userId);

    /**
     * @param Barcode $barcode
     */
    public function save(Barcode $barcode) : void;

    /**
     * @return array
     */
    public function all(): array;
}