<?php

namespace Tickets\Domain\Model\Barcode;

use Tickets\Domain\Model\Ticket\Ticket;

class Barcode
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * @var string
     */
    private $barcode;

    /**
     * Barcode constructor.
     * @param Ticket $ticket
     * @param string $barcode
     */
    public function __construct(Ticket $ticket, string $barcode)
    {
        $this->ticket = $ticket;

        $this->setBarcode($barcode);
    }

    /**
     * @param string $barcode
     * @throws EmptyBarcodeException
     */
    private function setBarcode(string $barcode): void
    {
        if (empty($barcode)) {
            throw new EmptyBarcodeException();
        }

        $this->barcode = $barcode;
    }

    /**
     * @return int
     */
    public function id() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Ticket
     */
    public function ticket()
    {
        return $this->ticket;
    }

    /**
     * @return string
     */
    public function barcode()
    {
        return $this->barcode;
    }
}