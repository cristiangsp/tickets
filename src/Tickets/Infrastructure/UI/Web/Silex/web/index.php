<?php

require_once __DIR__ . '/../../../../../../../vendor/autoload.php';

use Tickets\Infrastructure\UI\Web\Silex\Form\Ticket\CreateListingWithTicketsFormType;
use Tickets\Infrastructure\UI\Web\Silex\Form\Barcode\CreateBarcodeFormType;
use Symfony\Component\HttpFoundation\Request;

$app = \Tickets\Infrastructure\UI\Web\Silex\Application::bootstrap();

$app->get("/", function() use ($app) {
    return $app['twig']->render('login/login.html.twig');
});

$app->get("/listings", function() use ($app) {

    $token = $app['security.token_storage']->getToken();
    $user = $token->getUser();

    $listingRepository = $app['listing.repository'];

    return $app['twig']->render('listing/list.html.twig', ["listings" => $listingRepository->ofUserId($user->id())]);
});

$app->match("/listings/create", function(Request $request) use ($app) {

    $form = $app['form.factory']->create(CreateListingWithTicketsFormType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $token = $app['security.token_storage']->getToken();
        $user = $token->getUser();

        $command = $form->getData();
        $command->setUserId($user->id());

        try {
            $app["command.bus"]->handle($command);
            return $app->redirect("/listings");

        } catch(\Tickets\Domain\Model\Listing\EmptyDescriptionException $exception) {
            $form->get('description')->addError(new \Symfony\Component\Form\FormError("Description cannot be empty"));
        } catch (\Tickets\Domain\Model\Listing\NegativeSellingPriceException $exception) {
            $form->get('sellingPrice')->addError(new \Symfony\Component\Form\FormError("Price must be positive"));
        } catch (\Tickets\Domain\Model\Listing\NegativeTicketsNumberException $exception) {
            $form->get('numTickets')->addError(new \Symfony\Component\Form\FormError("There must be at least one ticket"));
        }
    }

    return $app['twig']->render('listing/create.html.twig', ["form" => $form->createView()]);
});

$app->get("/listings/view/{listingId}", function($listingId) use ($app) {

    try {
        $listingRepository = $app['listing.repository'];
        $ticketRepository = $app['ticket.repository'];

        return $app['twig']->render(
            'listing/view.html.twig',
            [
                "listing" => $listingRepository->ofIdOrFail($listingId),
                "tickets" => $ticketRepository->ofListingId($listingId)
            ]
        );
    } catch (\Tickets\Domain\Model\Listing\DoesNotExistException $exception) {
        return $app->redirect("/listings");
    }
});


$app->match("/tickets/view/{ticketId}", function(Request $request, $ticketId) use ($app) {

    try {

        $form = $app['form.factory']->create(CreateBarcodeFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $token = $app['security.token_storage']->getToken();
            $user = $token->getUser();

            $command = $form->getData();
            $command->setTicketId($ticketId);
            $command->setUserId($user->id());

            try {
                $app["command.bus"]->handle($command);
                return $app->redirect("/tickets/view/$ticketId");

            } catch(\Tickets\Domain\Model\Ticket\DoesNotExistException $exception) {
                return $app->redirect("/listings");
            } catch(\Tickets\Domain\Model\Barcode\AlreadyExistsException $exception) {
                $form->get('barcode')->addError(new \Symfony\Component\Form\FormError("Barcode already exists"));
            }
        }

        $ticketRepository = $app['ticket.repository'];
        $barcodeRepository = $app['barcode.repository'];

        return $app['twig']->render(
            'ticket/view.html.twig',
            [
                "ticket" => $ticketRepository->ofIdOrFail($ticketId),
                "barcodes" => $barcodeRepository->ofTicketId($ticketId),
                "form" => $form->createView()
            ]
        );

    } catch (\Tickets\Domain\Model\Ticket\DoesNotExistException $exception) {
        return $app->redirect("/listings");
    }
});

$app->run();
