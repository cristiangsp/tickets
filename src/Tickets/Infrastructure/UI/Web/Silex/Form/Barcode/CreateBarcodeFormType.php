<?php

namespace Tickets\Infrastructure\UI\Web\Silex\Form\Barcode;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tickets\Application\Command\Barcode\CreateBarcodeCommand;

class CreateBarcodeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('barcode', TextType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CreateBarcodeCommand::class,
            'empty_data' => new CreateBarcodeCommand(0, 0, ""),
        ));
    }
}
