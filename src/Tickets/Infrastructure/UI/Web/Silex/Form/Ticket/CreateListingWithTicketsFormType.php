<?php

namespace Tickets\Infrastructure\UI\Web\Silex\Form\Ticket;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tickets\Application\Command\Listing\CreateListingWithTicketsCommand;

class CreateListingWithTicketsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class)
            ->add('sellingPrice', IntegerType::class)
            ->add('numTickets', IntegerType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CreateListingWithTicketsCommand::class,
            'empty_data' => new CreateListingWithTicketsCommand(0, 0, "", 0),
        ));
    }
}