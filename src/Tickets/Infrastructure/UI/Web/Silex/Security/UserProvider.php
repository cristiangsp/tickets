<?php

namespace Tickets\Infrastructure\UI\Web\Silex\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Tickets\Domain\Model\User\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Tickets\Domain\Model\User\DoesNotExistException;
use Tickets\Domain\Model\User\UserRepository;

class UserProvider implements UserProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserProvider constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     * @return User
     */
    public function loadUserByUsername($username)
    {
        try {
            $user = $this->userRepository->ofNameOrFail($username);
            $authenticatedUser = new \Tickets\Infrastructure\UI\Web\Silex\Security\User($user->name());
            $authenticatedUser->setId($user->id());
            return $authenticatedUser;

        } catch(DoesNotExistException $exception) {
            throw new UsernameNotFoundException("Username $username does not exist");
        }
    }

    /**
     * @param UserInterface $user
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'Tickets\Infrastructure\UI\Web\Silex\Security\User';
    }

}