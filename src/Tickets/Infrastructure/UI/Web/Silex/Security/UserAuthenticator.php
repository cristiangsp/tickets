<?php

namespace Tickets\Infrastructure\UI\Web\Silex\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Tickets\Domain\Model\User\DoesNotExistException;

class UserAuthenticator extends AbstractFormLoginAuthenticator
{
    /**
     * @param Request $request
     * @return mixed|void
     */
    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/login_check') {
            return;
        }

        return $request->request->get('login_user');
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            return $userProvider->loadUserByUsername($credentials['username']);
        } catch (DoesNotExistException $exception) {
            return null;
        }
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user) : bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return "/";
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse("/listings");
    }
}