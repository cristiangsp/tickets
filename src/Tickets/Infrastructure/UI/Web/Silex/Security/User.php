<?php

namespace Tickets\Infrastructure\UI\Web\Silex\Security;

use Symfony\Component\Security\Core\User\UserInterface;

class User extends \Tickets\Domain\Model\User\User implements UserInterface
{
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->name();
    }

    /**
     *
     */
    public function getPassword()
    {
    }

    /**
     *
     */
    public function eraseCredentials()
    {
    }

    /**
     *
     */
    public function getSalt()
    {
    }

    /**
     *
     */
    public function getRoles()
    {
        return [];
    }
}
