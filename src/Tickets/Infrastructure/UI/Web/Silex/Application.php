<?php

namespace Tickets\Infrastructure\UI\Web\Silex;

use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use League\Tactician\Plugins\LockingMiddleware;
use Tickets\Application\Command\Barcode\CreateBarcodeHandler;
use Tickets\Application\Command\Listing\CreateListingWithTicketsHandler;
use Tickets\Infrastructure\UI\Web\Silex\Form\Barcode\CreateBarcodeFormType;
use Tickets\Infrastructure\UI\Web\Silex\Form\Ticket\CreateListingWithTicketsFormType;
use Tickets\Infrastructure\UI\Web\Silex\Security\UserAuthenticator;
use Tickets\Infrastructure\UI\Web\Silex\Security\UserProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\TacticianServiceProvider;

class Application
{
    public static function bootstrap()
    {
        $app = new \Silex\Application();
        $app['debug'] = true;

        $app->register(new LocaleServiceProvider());
        $app->register(new TranslationServiceProvider(), array(
            'translator.domains' => array(),
        ));

        $app->register(new ValidatorServiceProvider());

        $app->register(new FormServiceProvider());

        $app->extend('form.types', function ($types) use ($app) {
            $types[] = new CreateListingWithTicketsFormType();
            $types[] = new CreateBarcodeFormType();
            return $types;
        });

        $app->register(new TwigServiceProvider(), array(
            'twig.path' => __DIR__ . '/views',
            'twig.form.templates' => ['bootstrap_3_layout.html.twig'],
        ));

        $app->register(new DoctrineServiceProvider, array(
            'db.options' => array(
                'driver'    => 'pdo_mysql',
                'host'      => 'dbserver',
                'dbname'    => 'tickets',
                'user'      => 'root',
                'password'  => 'tickets_password',
                'charset'   => 'utf8mb4',
            ),
        ));

        $app->register(new DoctrineOrmServiceProvider, array(
            'orm.proxies_dir' => __DIR__ . '/../../../../../../cache/doctrine/proxies',
            'orm.em.options' => array(
                'mappings' => array(
                    array(
                        'type' => 'yml',
                        'namespace' => 'Tickets\Domain\Model',
                        'path' => __DIR__.'/../../../Persistence/Doctrine/Mapping',
                    ),
                ),
            ),
        ));

        $app['user.repository'] = function ($app) {
            return $app['orm.em']->getRepository('Tickets\Domain\Model\User\User');
        };

        $app['listing.repository'] = function ($app) {
            return $app['orm.em']->getRepository('Tickets\Domain\Model\Listing\Listing');
        };

        $app['ticket.repository'] = function ($app) {
            return $app['orm.em']->getRepository('Tickets\Domain\Model\Ticket\Ticket');
        };

        $app['barcode.repository'] = function ($app) {
            return $app['orm.em']->getRepository('Tickets\Domain\Model\Barcode\Barcode');
        };

        $app->register(new SessionServiceProvider());

        $app['app.token_authenticator'] = function () {
            return new UserAuthenticator();
        };

        $app->register(new SecurityServiceProvider(), array(
            'security.firewalls' => array(
                'login' => array(
                    'pattern' => '^/$',
                ),
                'secured' => array(
                    'pattern' => '^.*$',
                    'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
                    'logout' => array('logout_path' => '/logout', 'invalidate_session' => true),
                    'guard' => array(
                        'authenticators' => array(
                            'app.token_authenticator'
                        ),
                    ),
                    'users' => function () use ($app) {
                        return new UserProvider($app['user.repository']);
                    },
                ),
            ))
        );

        $app['tactician.inflector'] = function () {
            return "handle";
        };

        $app['tactician.middlewares'] = function () {
            return [new LockingMiddleware()];
        };

        $app->register(
            new TacticianServiceProvider(
                [
                    'tactician.inflector' => "class_name",
                    'tactician.middlewares' =>
                        [
                            new LockingMiddleware()
                        ]
                ]
            )
        );

        $app[CreateListingWithTicketsHandler::class] = function() use ($app) {
            return new CreateListingWithTicketsHandler(
                $app['user.repository'],
                $app['listing.repository'],
                $app['ticket.repository']
            );
        };

        $app['app.handler.CreateListingWithTicketsCommand'] = function () use ($app) {
            return $app[CreateListingWithTicketsHandler::class];
        };

        $app[CreateBarcodeHandler::class] = function() use ($app) {
            return new CreateBarcodeHandler(
                $app['ticket.repository'],
                $app['barcode.repository']
            );
        };

        $app['app.handler.CreateBarcodeCommand'] = function () use ($app) {
            return $app[CreateBarcodeHandler::class];
        };

        return $app;
    }
}