<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Ticket;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Tickets\Domain\Model\Ticket\Ticket;

class TicketDataLoader extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $initialTicketsData = [
            [$this->getReference("Coldplay-500-listing"), null, null],
            [$this->getReference("Coldplay-500-listing"), null, null],
            [$this->getReference("Adelle-600-listing"), $this->getReference("Frank-user"), \DateTime::createFromFormat('Y-m-d H:i:s', "2016-20-01 10:00:00")],
            [$this->getReference("Adelle-650-listing"), $this->getReference("Hans-user"), \DateTime::createFromFormat('Y-m-d H:i:s', "2016-20-01 16:00:00")],
            [$this->getReference("Adelle-500-listing"), $this->getReference("Ruud-user"), \DateTime::createFromFormat('Y-m-d H:i:s', "2016-20-02 10:00:00")],
            [$this->getReference("Coldplay-600-listing"), $this->getReference("Frank-user"), \DateTime::createFromFormat('Y-m-d H:i:s', "2016-20-03 12:00:00")],
            [$this->getReference("Coldplay-660-listing"), $this->getReference("Ruud-user"), \DateTime::createFromFormat('Y-m-d H:i:s', "2016-20-03 12:05:00")],
        ];

        $ticketCounter = 1;

        foreach ($initialTicketsData as $ticketData) {
            $ticket = new Ticket($ticketData[0]);

            if ($ticketData[1] && $ticketData[2]) {
                $ticket->purchase($ticketData[1], $ticketData[2]);
            }

            $manager->persist($ticket);
            $this->addReference("ticket-$ticketCounter", $ticket);
            $ticketCounter++;
        }

        $manager->flush();
    }
}