<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Listing;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\User\User;

class ListingDataLoader extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $initialListingsData = [
            [$this->getReference("Frank-user"), 500, "Coldplay"],
            [$this->getReference("Frank-user"), 600, "Adelle"],
            [$this->getReference("Frank-user"), 650, "Adelle"],
            [$this->getReference("Frank-user"), 500, "Adelle"],
            [$this->getReference("Frank-user"), 600, "Coldplay"],
            [$this->getReference("Frank-user"), 660, "Coldplay"],
        ];

        foreach ($initialListingsData as $listingData) {
            $listing = new Listing($listingData[0], $listingData[1], $listingData[2]);
            $manager->persist($listing);
            $this->addReference($listingData[2] ."-" . $listingData[1] . "-listing", $listing);
        }

        $manager->flush();
    }
}