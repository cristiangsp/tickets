<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Fixtures;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Barcode\BarcodeDataLoader;
use Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Listing\ListingDataLoader;
use Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Ticket\TicketDataLoader;
use Tickets\Infrastructure\Persistence\Doctrine\Fixtures\User\UserDataLoader;

class LoadFixturesCommand extends Command
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * LoadFixturesCommand constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('app:load-data-fixtures')
            ->setDescription('Load data fixtures.')
            ->setHelp('This command loads all the included fixtures.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        $loader = new Loader();
        $loader->addFixture(new UserDataLoader());
        $loader->addFixture(new ListingDataLoader());
        $loader->addFixture(new TicketDataLoader());
        $loader->addFixture(new BarcodeDataLoader());

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);

        $executor->execute($loader->getFixtures());

        $output->writeln("Fixtures loaded properly");
    }
}
