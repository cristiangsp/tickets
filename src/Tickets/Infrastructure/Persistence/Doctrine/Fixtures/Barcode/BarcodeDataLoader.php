<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Fixtures\Barcode;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Tickets\Domain\Model\Barcode\Barcode;

class BarcodeDataLoader extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $initialBarcodesData = [
            [$this->getReference("ticket-1"), "EAN-13:111111111"],
            [$this->getReference("ticket-1"), "EAN-13:222222222"],
            [$this->getReference("ticket-2"), "EAN-13:333333333"],
            [$this->getReference("ticket-3"), "EAN-13:444444444"],
            [$this->getReference("ticket-4"), "EAN-13:555555555"],
            [$this->getReference("ticket-5"), "EAN-13:666666666"],
            [$this->getReference("ticket-5"), "EAN-13:777777777"],
            [$this->getReference("ticket-6"), "EAN-13:888888888"],
            [$this->getReference("ticket-7"), "EAN-13:888888888"]
        ];

        foreach ($initialBarcodesData as $barcodeData) {
            $barcode = new Barcode($barcodeData[0], $barcodeData[1]);
            $manager->persist($barcode);
        }

        $manager->flush();
    }
}