<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Fixtures\User;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Tickets\Domain\Model\User\User;

class UserDataLoader extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $initialUsernames = [
            "Frank",
            "Ruud",
            "Hans",
            "Joe"
        ];

        foreach ($initialUsernames as $username) {
            $user = new User($username);
            $manager->persist($user);
            $this->addReference("$username-user", $user);
        }

        $manager->flush();
    }
}