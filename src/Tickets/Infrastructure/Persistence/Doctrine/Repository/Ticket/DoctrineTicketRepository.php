<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Repository\Ticket;

use Doctrine\ORM\EntityRepository;
use Tickets\Domain\Model\Ticket\DoesNotExistException;
use Tickets\Domain\Model\Ticket\Ticket;
use Tickets\Domain\Model\Ticket\TicketRepository;

class DoctrineTicketRepository extends EntityRepository implements TicketRepository
{
    /**
     * @return array
     */
    public function all(): array
    {
        return $this->findAll();
    }

    /**
     * @param Ticket $ticket
     */
    public function save(Ticket $ticket): void
    {
        $this->getEntityManager()->persist($ticket);
        $this->getEntityManager()->flush();
    }

    /**
     * @param int $ticketId
     * @return Ticket
     * @throws DoesNotExistException
     */
    public function ofIdOrFail(int $ticketId): Ticket
    {
        $ticket = $this->find($ticketId);

        if (!$ticket) {
            throw new DoesNotExistException();
        }

        return $ticket;
    }

    /**
     * @param int $listingId
     * @return array
     */
    public function ofListingId(int $listingId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT t FROM Tickets\Domain\Model\Ticket\Ticket t WHERE t.listing = $listingId"
        );
        return $query->getResult();
    }
}
