<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Repository\Barcode;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Tickets\Domain\Model\Barcode\BarcodeRepository;
use Tickets\Domain\Model\Barcode\DoesNotExistException;
use Tickets\Domain\Model\Barcode\Barcode;

class DoctrineBarcodeRepository extends EntityRepository implements BarcodeRepository
{
    /**
     * @return array
     */
    public function all(): array
    {
        return $this->findAll();
    }

    /**
     * @param Barcode $barcode
     */
    public function save(Barcode $barcode): void
    {
        $this->getEntityManager()->persist($barcode);
        $this->getEntityManager()->flush();
    }

    /**
     * @param $id
     * @return Barcode
     * @throws DoesNotExistException
     */
    public function ofIdOrFail($id): Barcode
    {
        $barcode = $this->find($id);

        if (!$barcode) {
            throw new DoesNotExistException();
        }

        return $barcode;
    }

    /**
     * @param int $ticketId
     * @return array
     */
    public function ofTicketId(int $ticketId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT b FROM Tickets\Domain\Model\Barcode\Barcode b WHERE b.ticket = $ticketId"
        );
        return $query->getResult();
    }

    /**
     * @param $code
     * @param $userId
     * @return Barcode|null
     */
    public function withCodeAndNotBoughtByUserId($code, $userId)
    {
        $query =$this->getEntityManager()->createQuery(
            "SELECT b 
                  FROM Tickets\Domain\Model\Barcode\Barcode b 
                  INNER JOIN b.ticket t WITH t.boughtByUser != $userId OR t.boughtByUser IS NULL 
                  WHERE b.barcode = '$code'"
        );

        return $query->getResult();
    }
}
