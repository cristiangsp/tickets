<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Repository\User;

use Doctrine\ORM\EntityRepository;
use Tickets\Domain\Model\User\DoesNotExistException;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\User\UserRepository;

class DoctrineUserRepository extends EntityRepository implements UserRepository
{
    /**
     * @param $id
     * @return User
     * @throws DoesNotExistException
     */
    public function ofIdOrFail($id): User
    {
        $user = $this->find($id);

        if (!user) {
            throw new DoesNotExistException();
        }

        return $user;
    }

    /**
     * @param $name
     * @return User
     * @throws DoesNotExistException
     */
    public function ofNameOrFail($name): User
    {
        $user = $this->findOneBy(["name" => $name]);

        if (!$user) {
            throw new DoesNotExistException();
        }

        return $user;
    }

    /**
     * @return array
     */
    public function all() : array
    {
        return $this->findAll();
    }

    /**
     * @param User $user
     */
    public function save(User $user) : void
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
}