<?php

namespace Tickets\Infrastructure\Persistence\Doctrine\Repository\Listing;

use Doctrine\ORM\EntityRepository;
use Tickets\Domain\Model\Listing\DoesNotExistException;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\Listing\ListingRepository;

class DoctrineListingRepository extends EntityRepository implements ListingRepository
{
    /**
     * @return array
     */
    public function all(): array
    {
        return $this->findAll();
    }

    /**
     * @param Listing $listing
     */
    public function save(Listing $listing): void
    {
        $this->getEntityManager()->persist($listing);
        $this->getEntityManager()->flush();
    }

    /**
     * @param $id
     * @return Listing
     * @throws DoesNotExistException
     */
    public function ofIdOrFail($id): Listing
    {
        $listing = $this->find($id);

        if (!$listing) {
            throw new DoesNotExistException();
        }

        return $listing;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function ofUserId(int $userId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT l FROM Tickets\Domain\Model\Listing\Listing l WHERE l.author = $userId"
        );
        return $query->getResult();
    }
}