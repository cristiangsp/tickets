<?php

namespace Tickets\Infrastructure\Persistence\InMemory\Ticket;

use Tickets\Domain\Model\Ticket\DoesNotExistException;
use Tickets\Domain\Model\Ticket\Ticket;
use Tickets\Domain\Model\Ticket\TicketRepository;

class InMemoryTicketRepository implements TicketRepository
{
    /**
     * @var array
     */
    private $tickets;

    /**
     * InMemoryTicketRepository constructor.
     */
    public function __construct()
    {
        $this->tickets = [];
    }

    /**
     * @param Ticket $ticket
     */
    public function save(Ticket $ticket): void
    {
        $ticket->setId(count($this->tickets) + 1);
        $this->tickets[] = $ticket;
    }

    /**
     * @return array
     */
    public function all() : array
    {
        return $this->tickets;
    }

    /**
     * @param int $ticketId
     * @return Ticket
     * @throws DoesNotExistException
     */
    public function ofIdOrFail(int $ticketId): Ticket
    {
        foreach ($this->tickets as $ticket) {
            if ($ticket->id() === $ticketId) {
                return $ticket;
            }
        }

        throw new DoesNotExistException();
    }

    /**
     * @param int $listingId
     * @return array
     */
    public function ofListingId(int $listingId): array
    {
        $tickets = [];

        foreach ($this->tickets as $ticket) {
            if ($ticket->listing()->id() === $listingId) {
                $tickets[] = $ticket;
            }
        }

        return $tickets;
    }
}
