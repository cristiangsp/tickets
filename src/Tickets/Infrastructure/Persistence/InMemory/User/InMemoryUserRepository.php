<?php

namespace Tickets\Infrastructure\Persistence\InMemory\User;

use Tickets\Domain\Model\User\DoesNotExistException;
use Tickets\Domain\Model\User\User;
use Tickets\Domain\Model\User\UserRepository;

class InMemoryUserRepository implements UserRepository
{
    /**
     * @var array
     */
    private $users;

    /**
     * InMemoryUserRepository constructor
     */
    public function __construct()
    {
        $this->users = [];
    }

    /**
     * @param $id
     * @return User
     * @throws DoesNotExistException
    */
    public function ofIdOrFail($id): User
    {
        foreach ($this->users as $user) {
            if ($user->id() === $id) {
                return $user;
            }
        }

        throw new DoesNotExistException();
    }

    /**
     * @param $name
     * @return User
     * @throws DoesNotExistException
     */
    public function ofNameOrFail($name): User
    {
        foreach ($this->users as $user) {
            if ($user->name() === $name) {
                return $user;
            }
        }

        throw new DoesNotExistException();
    }

    /**
     * @param User $user
     */
    public function save(User $user): void
    {
        $user->setId(count($this->users) + 1);
        $this->users[] = $user;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->users;
    }
}