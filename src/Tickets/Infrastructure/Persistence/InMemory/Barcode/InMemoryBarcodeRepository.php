<?php

namespace Tickets\Infrastructure\Persistence\InMemory\Barcode;

use Tickets\Domain\Model\Barcode\BarcodeRepository;
use Tickets\Domain\Model\Barcode\Barcode;
use Tickets\Domain\Model\Barcode\DoesNotExistException;

class InMemoryBarcodeRepository implements BarcodeRepository
{
    /**
     * @var array
     */
    private $barcodes;

    /**
     * InMemoryBarcodeRepository constructor.
     */
    public function __construct()
    {
        $this->barcodes = [];
    }

    /**
     * @param Barcode $barcode
     */
    public function save(Barcode $barcode): void
    {
        $barcode->setId(count($this->barcodes) + 1);
        $this->barcodes[] = $barcode;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->barcodes;
    }

    /**
     * @param $id
     * @return Barcode
     * @throws DoesNotExistException
     */
    public function ofIdOrFail($id): Barcode
    {
        foreach ($this->barcodes as $barcode) {
            if ($barcode->id() === $id) {
                return $barcode;
            }
        }

        throw new DoesNotExistException();
    }


    /**
     * @param int $ticketId
     * @return array
     */
    public function ofTicketId(int $ticketId): array
    {
        $barcodes = [];

        foreach ($this->barcodes as $barcode) {
            if ($barcode->ticket()->id() === $ticketId) {
                $barcodes[] = $barcode;
            }
        }

        return $barcodes;
    }

    /**
     * @param $code
     * @param $userId
     * @return Barcode|null
     */
    public function withCodeAndNotBoughtByUserId($code, $userId)
    {
        foreach ($this->barcodes as $barcode) {
            if ($barcode->barcode() === $code) {
                if (!$barcode->ticket()->wasBoughtBy($userId)) {
                    return $barcode;
                }
            }
        }

        return null;
    }
}

