<?php

namespace Tickets\Infrastructure\Persistence\InMemory\Listing;

use Tickets\Domain\Model\Listing\DoesNotExistException;
use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\Listing\ListingRepository;

class InMemoryListingRepository implements ListingRepository
{
    /**
     * @var array
     */
    private $listings;

    /**
     * InMemoryListingRepository constructor.
     */
    public function __construct()
    {
        $this->listings = [];
    }

    /**
     * @param Listing $listing
     */
    public function save(Listing $listing): void
    {
        $listing->setId(count($this->listings) + 1);
        $this->listings[] = $listing;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->listings;
    }

    /**
     * @param $id
     * @return Listing
     * @throws DoesNotExistException
     */
    public function ofIdOrFail($id): Listing
    {
        foreach ($this->listings as $listing) {
            if ($listing->id() === $id) {
                return listing;
            }
        }

        throw new DoesNotExistException();
    }


    /**
     * @param int $userId
     * @return array
     */
    public function ofUserId(int $userId): array
    {
        $listings = [];

        foreach ($this->listings as $listing) {
            if ($listing->author()->id() === $userId) {
                $listings[] = $listing;
            }
        }

        return $listings;
    }
}
