<?php

namespace Tickets\Application\Command\Barcode;

class CreateBarcodeCommand
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $ticketId;

    /**
     * @var string
     */
    private $barcode;

    /**
     * CreateBarcodeCommand constructor.
     * @param int $userId
     * @param int $ticketId
     * @param string $barcode
     */
    public function __construct(int $userId, int $ticketId, string $barcode)
    {
        $this->userId = $userId;
        $this->ticketId = $ticketId;
        $this->barcode = $barcode;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId) : void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function userId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function ticketId()
    {
        return $this->ticketId;
    }

    /**
     * @param $ticketId
     */
    public function setTicketId($ticketId) : void
    {
        $this->ticketId = $ticketId;
    }

    /**
     * @return string
     */
    public function barcode()
    {
        return $this->barcode;
    }

    /**
     * @param $barcode
     */
    public function setBarcode($barcode) : void
    {
        $this->barcode = $barcode;
    }
}