<?php


namespace Tickets\Application\Command\Barcode;

use Tickets\Domain\Model\Barcode\AlreadyExistsException;
use Tickets\Domain\Model\Barcode\BarcodeRepository;
use Tickets\Domain\Model\Ticket\TicketRepository;
use Tickets\Domain\Model\Barcode\Barcode;

class CreateBarcodeHandler
{
    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     * @var BarcodeRepository
     */
    private $barcodeRepository;

    /**
     * @param TicketRepository $ticketRepository
     * @param BarcodeRepository $barcodeRepository
     */
    public function __construct(TicketRepository $ticketRepository, BarcodeRepository $barcodeRepository)
    {
        $this->ticketRepository = $ticketRepository;
        $this->barcodeRepository = $barcodeRepository;
    }

    /**
     * @param CreateBarcodeCommand $command
     * @throws AlreadyExistsException
     */
    public function handle(CreateBarcodeCommand $command)
    {
        if ($this->barcodeRepository->withCodeAndNotBoughtByUserId($command->barcode(), $command->userId())) {
            throw new AlreadyExistsException();
        }

        $ticket = $this->ticketRepository->ofIdOrFail($command->ticketId());
        $this->barcodeRepository->save(new Barcode($ticket, $command->barcode()));
    }
}