<?php

namespace Tickets\Application\Command\Listing;

class CreateListingWithTicketsCommand
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $sellingPrice;

    /**
     * @var description
     */
    private $description;

    /**
     * @var int
     */
    private $numTickets;

    /**
     * CreateListingWithTicketsCommand constructor.
     *
     * @param int $userId
     * @param int $sellingPrice
     * @param string $description
     * @param int $numTickets
     */
    public function __construct(int $userId, int $sellingPrice, string $description, int $numTickets)
    {
        $this->userId = $userId;
        $this->sellingPrice = $sellingPrice;
        $this->description = $description;
        $this->numTickets = $numTickets;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId) : void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function userId() : int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function sellingPrice() : int
    {
        return $this->sellingPrice;
    }

    /**
     * @param int $sellingPrice
     */
    public function setSellingPrice($sellingPrice) : void
    {
        $this->sellingPrice = $sellingPrice;
    }

    /**
     * @return string
     */
    public function description() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description) : void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function numTickets() : int
    {
        return $this->numTickets;
    }

    /**
     * @param int $numTickets
     */
    public function setNumTickets($numTickets) : void
    {
        $this->numTickets = $numTickets;
    }

}