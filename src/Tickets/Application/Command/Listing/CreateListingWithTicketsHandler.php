<?php

namespace Tickets\Application\Command\Listing;

use Tickets\Domain\Model\Listing\Listing;
use Tickets\Domain\Model\Listing\NegativeTicketsNumberException;
use Tickets\Domain\Model\Ticket\Ticket;
use Tickets\Domain\Model\User\UserRepository;
use Tickets\Domain\Model\Listing\ListingRepository;
use Tickets\Domain\Model\Ticket\TicketRepository;

class CreateListingWithTicketsHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ListingRepository
     */
    private $listingRepository;

    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     * CreateListingWithTicketsHandler constructor.
     *
     * @param UserRepository $userRepository
     * @param ListingRepository $listingRepository
     * @param TicketRepository $ticketRepository
     */
    public function __construct(
        UserRepository $userRepository,
        ListingRepository $listingRepository,
        TicketRepository $ticketRepository
    ) {
        $this->userRepository = $userRepository;
        $this->listingRepository = $listingRepository;
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * @param CreateListingWithTicketsCommand $command
     * @return Listing
     */
    public function handle(CreateListingWithTicketsCommand $command) : Listing
    {
        $listing = $this->createListing($command);
        $this->createTickets($listing, $command->numTickets());

        return $listing;
    }

    /**
     * @param CreateListingWithTicketsCommand $command
     * @return Listing
     */
    private function createListing(CreateListingWithTicketsCommand $command)
    {
        $user = $this->userRepository->ofIdOrFail($command->userId());

        $listing = new Listing($user, $command->sellingPrice(), $command->description());
        $this->listingRepository->save($listing);

        return $listing;
    }

    /**
     * @param Listing $listing
     * @param int $numTickets
     * @throws NegativeTicketsNumberException
     */
    private function createTickets(Listing $listing, int $numTickets)
    {
        if ($numTickets <= 0) {
            throw new NegativeTicketsNumberException();
        }

        for ($ticketCounter = 0; $ticketCounter < $numTickets; $ticketCounter++ ) {
            $this->ticketRepository->save(new Ticket($listing));
        }
    }
}
